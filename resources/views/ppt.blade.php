@extends('layouts.front-end')
@section('content')
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-caption">
          <h2 class="page-title">Paediatric Physical Therapy</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-breadcrumb">
  <div class="container">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('index')}}">Home</a></li>
        <li class="active">Paediatric Physical Therapy</li>
      </ol>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1>Paediatric Physical Therapy</h1>
        <img src="{{ url('images/service-single.jpg')}}" alt="" class="img-responsive mb30">
        <p class="lead">Paediatric physical therapy assists in early detection of health problems as well as the diagnosis, treatment, and management of infants, children, and adolescents with a variety of injuries, disorders, and diseases that affect the muscles, bones, and joints. Treatments focus on improving gross and fine motor skills, balance and coordination, strength and endurance as well as cognitive and sensory processing and integration. Children with developmental delays, cerebral palsy, spina bifida and torticollis are a few of the patients treated by paediatric physical therapists.​</p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="sidenav">
          <ul class="listnone">
            <li> <a href="{{ route('ppt')}}">Paediatric Physical Therapy</a></li>
            <li> <a href="{{ route('npt')}}">Neurological Physical Therapy</a></li>
            <li> <a href="{{ route('gpt')}}">Geriatric Physical Therapy</a></li>
            <li> <a href="{{ route('opt')}}" class="active">Orthopaedic Physical Therapy</a></li>
            <li> <a href="{{ route('women_health') }}">Women Health</a></li>
            <li> <a href="{{ route('pfrapm')}}">Physiotherapy For Rehabilitation And Pain Management</a></li>
          </ul>
        </div>
        <div class="widget widget-call-to-action">
          <h1 class="widget-title">Book Your Appointment</h1>
          <a href="#" class="btn btn-white btn-lg">Make An Appointment</a>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.appointment-footer')
@stop
