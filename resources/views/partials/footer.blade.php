<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="footer-widget widget-newsletter">
                    <h2 class="widget-title">Newsletters</h2>
                    <p>Enter your email address to receive new patient information and other useful information right to your inbox.</p>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Email Address">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Subscribe</button>
                       </span>
                    </div>
                    <!-- /input-group -->
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                <div class="footer-widget">
                    <h2 class="widget-title">Therapies</h2>
                    <ul class="listnone">
                      <li> <a href="{{ route('ppt')}}">Paediatric Physical Therapy</a></li>
                      <li> <a href="{{ route('npt')}}">Neurological Physical Therapy</a></li>
                      <li> <a href="{{ route('gpt')}}">Geriatric Physical Therapy</a></li>
                      <li> <a href="{{ route('opt')}}" class="active">Orthopaedic Physical Therapy</a></li>
                      <li> <a href="{{ route('women_health') }}">Women Health</a></li>
                      <li> <a href="{{ route('pfrapm')}}">Physiotherapy For Rehabilitation And Pain Management</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="footer-widget footer-social">
                    <h2 class="widget-title">Social Feed</h2>
                    <ul class="listnone">
                        <li><a href="https://www.facebook.com/swasthealth"> <i class="fa fa-facebook"></i> Facebook  </a></li>
                        <li><a href="https://twitter.com/swasthealth"><i class="fa fa-twitter"></i> Twitter</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-2 col-sm-2 col-xs-12">
                <div class="footer-widget footer-social">
                    <h2 class="widget-title">Clinic Address</h2>
                    <ul class="listnone contact">
                        <li><i class="fa fa-map-marker"></i> 17 Raja Apartment, Godavari Mahtre Marg,Near Vitthal Mandir, Dahisar West,Mumbai - 400068
                        </li>
                        <li><i class="fa fa-phone"></i>7045063537 / 8850663700</li>
                        <li><i class="fa fa-envelope-o"></i> swasthealth@gmail.com</li>
                    </ul>
                </div>
            </div>


        </div>
    </div>


</div>
 <div class="tiny-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="copyright-content">
              <p>© Swasth Health  |  All rights reserved</p>
            </div>
          </div>
        </div>
      </div>
    </div>
