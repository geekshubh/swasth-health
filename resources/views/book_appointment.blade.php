@extends('layouts.front-end')
@section('content')
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-caption">
          <h2 class="page-title">Book An Appointment</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-breadcrumb">
  <div class="container">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('index')}}">Home</a></li>
        <li class="active">Book an Appointment</li>
      </ol>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class = "card-header">
            <h2>
              Book Appointment
              @if(Session::has('success'))
              <p class="alert alert-info">{{ Session::get('success') }}</p>
              @elseif(Session::has('errors'))
              <p class="alert alert-danger">{{ Session::get('errors')}}</p>
              @endif
            </h2>
          </div>
          <div class = "card-body">
            <form action = "{{ route('save-appointment')}}" method = "POST">
              @csrf
              <div class="form-group">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" placeholder="First Name" name ="first_name">
              </div>
              <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" placeholder="Last Name" name ="last_name">
              </div>
              <div class="form-group">
                <label for="phone_number">Phone Number</label>
                <input type="text" class="form-control" id="phone_number" placeholder="Phone Number" name= "contact_number">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name = "email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
              <div class="form-group">
                <div class="input-group date" id="datetimepicker3">
                  <label for="date_input">Select Date</label>
                    <input type='text' class="form-control" id="date_input" name="date" onchange="dropdown(this.value);"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
              </div>
              <div class="form-group">
                  <label for="time_input">Select Time</label>
                  <select class="form-control" name="time" id="time_input">
                    <option>Please choose the date from above dropdown</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="payment_type">Payment Mode</label>
                  <select class="form-control" name="payment_type" id="payment_type">
                    <option value="Offline">Offline Payment</option>
                    <option value="Online">Online Payment</option>
                  </select>
              </div>
              <div class="form-group">
                <label for="amount">Amount</label>
                <input type="text" class="form-control" id="amount" placeholder="Amount" value ="400" disabled>
              </div>

              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@include('partials.javascripts')
