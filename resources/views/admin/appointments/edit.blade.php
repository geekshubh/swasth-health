@extends('layouts.app')
@section('content')
<div class="content">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-header">
            Edit Appointment
            <a href="{{ route('appointments.index')}}" class="btn btn-md btn-danger float-right">Back To Appointments</a>
          </div>
          <div class="card-body">
            <form action = "{{ route('update-appointment',[$appointment->id])}}" method = "POST">
              <input name="_method" type="hidden" value="PUT">
              @csrf
              <div class="form-group">
                  <label for="payment_type">Payment Type</label>
                  <select class="form-control" name="payment_type" id="payment_type">
                    <option value="Offline">Offline</option>
                    <option value = "Online">Online</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="payment_status">Payment Status</label>
                  <select class="form-control" name="payment_status" id="payment_status">
                    <option value="Paid">Paid</option>
                    <option value = "Unpaid">Unpaid</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="appointment_status">Appointment Status</label>
                  <select class="form-control" name="appointment_status" id="appointment_status">
                    <option value="Initiated">Initiated</option>
                    <option value = "Confirmed">Confirmed</option>
                  </select>
              </div>
              <button type ="submit" class="btn btn-md btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
