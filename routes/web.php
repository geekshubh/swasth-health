<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses'=>'WebsiteController@index','as'=>'index']);
Route::get('/orthopaedic-physical-therapy',['uses'=>'WebsiteController@opt','as'=>'opt']);
Route::get('/geriatric-physical-therapy',['uses'=>'WebsiteController@gpt','as'=>'gpt']);
Route::get('/neurological-physical-therapy',['uses'=>'WebsiteController@npt','as'=>'npt']);
Route::get('/paediatric-physical-therapy',['uses'=>'WebsiteController@ppt','as'=>'ppt']);
Route::get('/women-health',['uses'=>'WebsiteController@women_health','as'=>'women_health']);
Route::get('/physiotherapy-for-rehabilition-and-pain-management',['uses'=>'WebsiteController@pfrapm','as'=>'pfrapm']);
Route::get('book-appointment',['uses'=>'WebsiteController@book_appointment','as'=>'book-appointment']);

Route::get('get-time/{date}',['uses'=>'AppointmentsController@check_time','as'=>'get-time']);
Route::post('save-appointment',['uses'=>'AppointmentsController@book_appointment','as'=>'save-appointment']);
Route::get('admin/appointments',['uses'=>'AppointmentsController@index','as'=>'appointments.index'])->middleware('auth');
Route::get('admin/appointments/{id}/edit',['uses'=>'AppointmentsController@edit','as'=>'appointments.edit'])->middleware('auth');
Route::get('admin/appointments/{id}/view',['uses'=>'AppointmentsController@show','as'=>'appointments.show'])->middleware('auth');
Route::put('admin/appointments/{id}/update-appointment',['uses'=>'AppointmentsController@update','as'=>'update-appointment'])->middleware('auth');

Route::post('payment/status', ['uses'=>'AppointmentsController@payment_callback','as'=>'payment-callback']);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
/*$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');*/
