<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
  public function index()
  {
    return view('index');
  }
  public function opt()
  {
    return view('opt');
  }
  public function gpt()
  {
    return view('gpt');
  }
  public function npt()
  {
    return view('npt');
  }

  public function ppt()
  {
    return view('ppt');
  }

  public function women_health()
  {
    return view('women_health');
  }

  public function pfrapm()
  {
    return view('pfrapm');
  }

  public function book_appointment()
  {
    return view('book_appointment');
  }
}
