<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appointment_id')->unsigned();
            $table->text('transaction_id')->nullable();
            $table->text('order_id')->nullable();
            $table->text('bank_transaction_id')->nullable();
            $table->text('transaction_amount')->nullable();
            $table->text('currency')->nullable();
            $table->text('status')->nullable();
            $table->text('response_code')->nullable();
            $table->text('transaction_date_time')->nullable();
            $table->text('gateway_name')->nullable();
            $table->text('bank_name')->nullable();
            $table->text('payment_mode')->nullable();
            $table->text('checksum_hash')->nullable();
            $table->foreign('appointment_id')->references('id')->on('appointments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
