@extends('layouts.front-end')
@section('content')
<div class="hero-section">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h1 class="hero-title">Swasth Health</h1>
        <p class="hero-text">The Best Physiotherapy Clinic in Mumbai
        </p>
        <a href="{{ route('book-appointment')}}" class="btn btn-default">Book an Appointment Today</a>
      </div>
    </div>
  </div>
</div>
<div class="space-medium">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8">
        <div class="mb60 text-center section-title">
          <!-- section title start-->
          <h5>Physiotherapy Services</h5>
          <h1>Wide Range of Physical Therapy Services</h1>
        </div>
        <!-- /.section title start-->
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="service-block">
          <!-- service block -->
          <div class="service-img mb20">
            <!-- service img -->
            <a href="{{ route('opt')}}"><img src="{{ url('images/service-1.jpg')}}" alt="" class="img-responsive"></a>
          </div>
          <!-- service img -->
          <div class="service-content">
            <!-- service content -->
            <h2><a href="{{ route('opt')}}" class="title">ORTHOPAEDIC PHYSICAL THERAPY</a></h2>
            <p>Orthopaedic (physical therapist) diagnose, manage and treat disorders and injuries of the musculoskeletal system. They also help people recover from orthopaedic surgery. This specialty of physical therapy is most often found in the out-patient clinical setting.</p>
          </div>
          <!-- service content -->
        </div>
        <!-- /.service block -->
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="service-block">
          <!-- service block -->
          <div class="service-img mb20">
            <!-- service img -->
            <a href="{{ route('gpt')}}"><img src="{{ url('images/service-2.jpg')}}" alt="" class="img-responsive"></a>
          </div>
          <!-- service img -->
          <div class="service-content">
            <!-- service content -->
            <h2><a href="{{ route('gpt')}}" class="title">GERIATRIC PHYSICAL THERAPY</a></h2>
            <p>Geriatric physical therapy covers numerous issues concerning people as they go through normal adult aging</p>
          </div>
          <!-- service content -->
        </div>
        <!-- /.service block -->
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="service-block">
          <!-- service block -->
          <div class="service-img mb20">
            <!-- service img -->
            <a href="{{ route('npt')}}"><img src="{{ url('images/service-3.jpg')}}" alt="" class="img-responsive"></a>
          </div>
          <!-- service img -->
          <div class="service-content">
            <!-- service content -->
            <h2><a href="{{ route('npt')}}" class="title">NEUROLOGICAL PHYSICAL THERAPY</a></h2>
            <p>Neurological physical therapists work with individuals who have a neurological disorder or disease.</p>
          </div>
          <!-- service content -->
        </div>
        <!-- /.service block -->
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="service-block">
          <!-- service block -->
          <div class="service-img mb20">
            <!-- service img -->
            <a href="{{ route('ppt')}}"><img src="{{ url('images/service-4.jpg')}}" alt="" class="img-responsive"></a>
          </div>
          <!-- service img -->
          <div class="service-content">
            <!-- service content -->
            <h2><a href="{{ route('ppt')}}" class="title">PAEDIATRIC PHYSICAL THERAPY</a></h2>
            <p>Paediatric physical therapy assists in early detection of health problems as well as the diagnosis, treatment, and management of infants, children, and adolescents with a variety of injuries, disorders, and diseases that affect the muscles, bones, and joints.</p>
          </div>
          <!-- service content -->
        </div>
        <!-- /.service block -->
      </div>
    </div>
  </div>
</div>
<div class="space-small bg-primary">
  <!-- call to action -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
        <h1 class="cta-title">Book your online appointment</h1>
        <p class="cta-text"></p>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
        <a href="{{ route('book-appointment')}}" class="btn btn-white btn-lg mt20">Book Now</a>
      </div>
    </div>
  </div>
</div>
<!-- /.call to action -->
<div class="space-medium bg-default">
  <div class="container">
    <div class="row">
      <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
        <div class="section-title mb60 text-center">
          <!-- section title start-->
          <h1>Our Patient’s Review</h1>
        </div>
        <!-- /.section title start-->
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="testimonial-block">
          <!-- testimonial block -->
          <div class="testimonial-content">
            <p class="testimonial-text">the basis or her information given and her best advice ..i just want to say thanks alottt ....she is a superb physio with a very friendly manner.I thoroughly recommend Dr Dimple Mam”</p>
          </div>
          <div class="testimonial-info">
            <h4 class="testimonial-name">Seema Jaiswal</h4>
          </div>
        </div>
        <!--/. testimonial block -->
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="testimonial-block">
          <!-- testimonial block -->
          <div class="testimonial-content">
            <p class="testimonial-text">“Any place you go for any service, need an individual who is good listener and compassionate being....this is the fundamental characteristics of Dimple Modi....Thanks for your kindness during my knee pains and supporting me to recover”</p>
          </div>
          <div class="testimonial-info">
            <h4 class="testimonial-name">Yogi Arjun</h4>
          </div>
        </div>
      </div>
    </div>
    <br><br>
      <div class="row">
        <h1>Video Reviews</h1>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <!--/. testimonial block -->
        <div class="testimonial-block">
          <!-- testimonial block -->
          <div class="testimonial-content">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/LrJUwKy8Ogg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="testimonial-block">
          <!-- testimonial block -->
          <div class="testimonial-content">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/2qZzwePkpXM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
@stop
