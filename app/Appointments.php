<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    protected $table = "appointments";
    protected $fillable = ['order_id','first_name','last_name','contact_number','email','customer_id','date','time','amount','appointment_status','payment_type','payment_status','checksum_hash'];
}
