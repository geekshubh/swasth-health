@extends('layouts.app')
@section('content')
<div class="content">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-header">
            All Appointments
            @if(Session::has('success'))
            <p class="alert alert-info">{{ Session::get('success') }}</p>
            @endif
          </div>
          <div class="card-body">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Payment Status</th>
                      <th>Payment Type</th>
                      <th>Options</th>
                  </tr>
              </thead>
              <tbody>
                @if(count($appointments) > 0)
                @foreach($appointments as $appointment)
                  <tr>
                      <td>{{ $appointment->first_name }} {{ $appointment->last_name }}</td>
                      <td>{{ $appointment->date}}</td>
                      <td>{{ $appointment->time }}</td>
                      <td>{{ $appointment->payment_status }}</td>
                      <td>{{ $appointment->payment_type }}</td>
                      <td><a href="{{ route('appointments.edit',[$appointment->id])}}" class="btn btn-md btn-info">Edit</a>
                      <a href="{{ route('appointments.show',[$appointment->id])}}" class="btn btn-md btn-info">View</a></td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td>No Entries in the table</td>
                  </tr>
                  @endif
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@include('partials.javascripts')
