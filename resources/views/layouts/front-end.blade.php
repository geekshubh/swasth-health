<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials.head')
  </head>
  <body>
    @include('partials.top_bar')
    @include('partials.nav')
    @yield('content')
    @include('partials.footer')
  </body>
</html>
