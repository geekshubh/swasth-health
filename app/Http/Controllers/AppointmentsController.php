<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointments;
use Auth;
use Carbon\Carbon;
use App\Payments;
use Illuminate\Support\Str;
use PaytmWallet;
class AppointmentsController extends Controller
{
  public function index()
  {
    $appointments = Appointments::all();
    //dd($appointments);
    return view('admin.appointments.index',compact('appointments'));
  }

  public function edit($id)
  {
    $appointment = Appointments::findOrFail($id);
    return view('admin.appointments.edit',compact('appointment'));
  }

  public function update(Request $request,$id)
  {
    $appointment = Appointments::findOrFail($id);
    $appointment->appointment_status = $request->appointment_status;
    $appointment->payment_type = $request->payment_type;
    $appointment->payment_status = $request->payment_status;
    $appointment->update();
    return redirect()->route('appointments.index')->with('success','Appointment Updated Successfully');
  }

  public function show($id)
  {
    $appointment = Appointments::findOrFail($id);
    return view('admin.appointments.show',compact('appointment'));
  }

  public function book_appointment(Request $request)
  {
    $appointment_date_check = Appointments::where('date',$request->date)->count();
    $appointment_date_time_check = Appointments::where('date',$request->date)->where('time',$request->time)->count();

    if($appointment_date_time_check == 0)
    {
        $appointment = new Appointments;
        $appointment->first_name = $request->first_name;
        $appointment->last_name = $request->last_name;
        //$carbon_date_preprocess = Carbon::parse($request->date);

        $appointment->date = $request->date;
        $appointment->time = $request->time;
        $appointment->email = $request->email;
        $appointment->contact_number = $request->contact_number;
        $appointment->appointment_status = 'Initiated';
        $appointment->amount = '400';
        $appointment->order_id = Str::uuid();
        $appointment->payment_type = $request->payment_type;
        $appointment->customer_id = $request->first_name.$request->last_name;
        $appointment->payment_status = 'Processing';
        $appointment->save();
        if($appointment->payment_type == 'Online')
        {
            //return redirect()->route('payment-processing');
            //dd('successful');
            $payment = PaytmWallet::with('receive');
            $payment->prepare([
            'order' => $appointment->order_id,
            'user' => $appointment->customer_id,
            'mobile_number' => $appointment->contact_number,
            'email' => $appointment->email,
            'amount' => $appointment->amount,
            'callback_url' => 'https://swasthealth.com/payment/status'
          ]);
          return $payment->receive();
        }
        else
        {
          return redirect()->back()->with('success','Appointment Booked Sucessfully');
        }
    }
    else
    {
      return redirect()->back()->with('errors','This slot has been already booked');
    }
  }
  public function payment_processing()
  {

  }
  public function payment_callback()
  {
    $transaction = PaytmWallet::with('receive');
    $response = $transaction->response();
    if($transaction->isSuccessful())
    {
      $order_id = $transaction->getOrderId();
      //dd($response);
      $appointment_id = Appointments::where('order_id',$order_id)->pluck('id')->first();
      $appointment = Appointments::findOrFail($appointment_id);
      $appointment->payment_status = 'Successful';
      $appointment->update();
      return redirect()->route('book-appointment')->with('success','Appointment booked and Payment done Successfully');
    }
    else if($transaction->isFailed())
    {
      $order_id = $transaction->getOrderId();
      //dd($response);
      $appointment_id = Appointments::where('order_id',$order_id)->pluck('id')->first();
      $appointment = Appointments::findOrFail($appointment_id);
      $appointment->payment_status = 'Failed';
      $appointment->update();
      return redirect()->route('book-appointment')->with('info','Appointment not booked and Payment failed');
    }
    else if($transaction->isOpen())
    {
      $order_id = $transaction->getOrderId();
      //dd($response);
      $appointment_id = Appointments::where('order_id',$order_id)->pluck('id')->first();
      $appointment = Appointments::findOrFail($appointment_id);
      $appointment->payment_status = 'In Process';
      $appointment->update();
      return redirect()->route('book-appointment')->with('info','Appointment not booked and Payment not Done');
    }

  }
  public function check_time($date)
  {
    $get_blocked_time = Appointments::where('date',$date)->pluck('time')->toArray();
    //echo 'success';
    $get_day = Carbon::parse($date)->format('dddd');
    if($get_day != 'Saturday')
    {
      $time = ['10:00','11:00','12:00','13:00','17:00','18:00','19:00','20:00'];
    }
    elseif($get_day == 'Saturday')
    {
      $time = ['10:00','11:00','12:00','13:00'];
    }
    $array_diff_1 = array_diff($get_blocked_time,$time);
    $array_diff_2 = array_diff($time,$get_blocked_time);

    $userData = array_merge($array_diff_1,$array_diff_2);
    echo json_encode($userData);
    exit;
  }

}
