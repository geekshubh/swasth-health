<div class="header-top">
    <!-- header-top-->
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <span class="text-block time-block">
                            <span class="time-text">Mon to Friday: <strong>10am - 1pm and 5pm - 8pm</strong> Saturday <strong>10 am - 1pm</strong> - Sunday: <strong>Closed</strong></span>
                </span>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12">
                <div class="top-text">
                    <span class="text-block call-block">
                           <span class="call-no">7045063537 / 8850663700</span>
                    </span>
                    <span class="text-block mail-block hidden-xs hidden-sm">
                            <span class="mail-text">swasthealth@gmail.com</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
