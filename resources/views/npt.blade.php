@extends('layouts.front-end')
@section('content')
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-caption">
          <h2 class="page-title">Neurological Physical Therapy</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-breadcrumb">
  <div class="container">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('index')}}">Home</a></li>
        <li class="active">Neurological Physical Therapy</li>
      </ol>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1>Neurological Physical Therapy</h1>
        <img src="{{ url('images/service-single.jpg')}}" alt="" class="img-responsive mb30">
        <p class="lead">Neurological physical therapists work with individuals who have a neurological disorder or disease. These include: </p>
          <ol type = 1 class="lead">
            <li>Alzheimer's disease</li>
            <li>ALS</li>
            <li>Brain injury</li>
            <li>Cerebral palsy</li>
            <li>Multiple sclerosis</li>
            <li>Parkinson's disease</li>
            <li>Spinal cord injury</li>
            <li>Stroke</li>
          </ol>
        <p class="lead">Common problems of patients with neurological disorders include paralysis, vision impairment, poor balance, difficulty walking and loss of independence. Therapists work with patients to improve these areas of dysfunction.</p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="sidenav">
          <ul class="listnone">
            <li> <a href="{{ route('ppt')}}">Paediatric Physical Therapy</a></li>
            <li> <a href="{{ route('npt')}}">Neurological Physical Therapy</a></li>
            <li> <a href="{{ route('gpt')}}">Geriatric Physical Therapy</a></li>
            <li> <a href="{{ route('opt')}}" class="active">Orthopaedic Physical Therapy</a></li>
            <li> <a href="{{ route('women_health') }}">Women Health</a></li>
            <li> <a href="{{ route('pfrapm')}}">Physiotherapy For Rehabilitation And Pain Management</a></li>
          </ul>
        </div>
        <div class="widget widget-call-to-action">
          <h1 class="widget-title">Book Your Appointment</h1>
          <a href="#" class="btn btn-white btn-lg">Make An Appointment</a>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.appointment-footer')
@stop
