@extends('layouts.front-end')
@section('content')
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-caption">
          <h2 class="page-title">Orthopaedic Physical Therapy</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-breadcrumb">
  <div class="container">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('index')}}">Home</a></li>
        <li class="active">Orthopaedic Physical Therapy</li>
      </ol>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1>Orthopaedic Physical Therapy</h1>
        <img src="{{ url('images/service-single.jpg')}}" alt="" class="img-responsive mb30">
        <p class="lead">Orthopaedic (physical therapist) diagnose, manage and treat disorders and injuries of the musculoskeletal system. They also help people recover from orthopaedic surgery. This specialty of physical therapy is most often found in the out-patient clinical setting. Orthopaedic therapists are trained in the treatment of post-operative joints, sports injuries, arthritis, and amputations, among other injuries and conditions. Joint mobilizations, strength training, hot packs and cold packs, and electrical stimulation are often used to speed recovery in the orthopaedic setting. Those who have suffered injury or disease affecting the muscles, bones, ligaments or tendons of the body may benefit from assessment by a physical therapist specialized in orthopaedics. Typical injuries treated by orthopaedic PTs may include:</p>
          <ol class="lead" type = 1>
            <li>Fractures</li>
            <li>Sprains</li>
            <li>Tendonitis</li>
            <li>Bursitis</li>
          </ol>
          <p class="lead">
          An orthopaedic physical therapist can prescribe the right exercises for your specific condition affecting your bones, muscles, or joints.
        </p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="sidenav">
          <ul class="listnone">
            <li> <a href="{{ route('ppt')}}">Paediatric Physical Therapy</a></li>
            <li> <a href="{{ route('npt')}}">Neurological Physical Therapy</a></li>
            <li> <a href="{{ route('gpt')}}">Geriatric Physical Therapy</a></li>
            <li> <a href="{{ route('opt')}}" class="active">Orthopaedic Physical Therapy</a></li>
            <li> <a href="{{ route('women_health') }}">Women Health</a></li>
            <li> <a href="{{ route('pfrapm')}}">Physiotherapy For Rehabilitation And Pain Management</a></li>
          </ul>
        </div>
        <div class="widget widget-call-to-action">
          <h1 class="widget-title">Book Your Appointment</h1>
          <a href="#" class="btn btn-white btn-lg">Make An Appointment</a>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.appointment-footer')
@stop
