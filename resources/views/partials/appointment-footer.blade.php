<div class="space-small bg-primary">
  <!-- call to action -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
        <h1 class="cta-title">Book your online appointment</h1>
        <p class="cta-text">Get the best Physiotherapy Service in Mumbai</p>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
        <a href="#" class="btn btn-white btn-lg mt20">Book Now!</a>
      </div>
    </div>
  </div>
</div>
