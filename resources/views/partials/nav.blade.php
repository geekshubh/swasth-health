<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <a href="{{ route('index')}}"><img src = "{{ url('images/logo-resized-1.jpg')}}" /></a>
            </div>
            <div class="col-lg-8 col-md-4 col-sm-12 col-xs-12">
                 <div class="navigation">
                    <div id="navigation">
                        <ul>
                            <li class="active"><a href="{{ route('index')}}" title="Home">Home</a></li>

                            <li class="has-sub"><a href="#" title="Service List">Services</a>
                                <ul>
                                  <li> <a href="{{ route('ppt')}}" title="Service List">Paediatric Physical Therapy</a></li>
                                  <li> <a href="{{ route('npt')}}" title="Service List">Neurological Physical Therapy</a></li>
                                  <li> <a href="{{ route('gpt')}}" title="Service List">Geriatric Physical Therapy</a></li>
                                  <li> <a href="{{ route('opt')}}" title="Service List">Orthopaedic Physical Therapy</a></li>
                                  <li> <a href="{{ route('women_health') }}" title="Service List">Women Health</a></li>
                                  <li> <a href="{{ route('pfrapm')}}" title="Service List">Physiotherapy For Rehabilitation And Pain Management</a></li>
                                </ul>
                            </li>
                            <li><a href="#" title="Contact Us">Contact</a> </li>
                            <li><a href="{{ route('book-appointment')}}">Book Appointment</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
