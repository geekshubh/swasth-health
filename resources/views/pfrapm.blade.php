@extends('layouts.front-end')
@section('content')
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-caption">
          <h2 class="page-title">Physiotherapy For Rehabilitation And Pain Management</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-breadcrumb">
  <div class="container">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('index')}}">Home</a></li>
        <li class="active">Physiotherapy For Rehabilitation And Pain Management</li>
      </ol>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1>Physiotherapy For Rehabilitation And Pain Management</h1>
        <img src="{{ url('images/service-single.jpg')}}" alt="" class="img-responsive mb30">
        <p class="lead">If you’re suffering from chronic pain, are due for surgery or if you’ve recently had surgery, this type of physiotherapy focuses on getting you back on track to a pain free lifestyle.Treatment prior to surgery focuses on exercises that will strengthen your body and prepare you for post-operative rehabilitation. After surgery or injury, reducing pain and swelling is the first priority and then gentle exercises to improve function with the aim of getting you back on track as soon as possible.</p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="sidenav">
          <ul class="listnone">
            <li> <a href="{{ route('ppt')}}">Paediatric Physical Therapy</a></li>
            <li> <a href="{{ route('npt')}}">Neurological Physical Therapy</a></li>
            <li> <a href="{{ route('gpt')}}">Geriatric Physical Therapy</a></li>
            <li> <a href="{{ route('opt')}}" class="active">Orthopaedic Physical Therapy</a></li>
            <li> <a href="{{ route('women_health') }}">Women Health</a></li>
            <li> <a href="{{ route('pfrapm')}}">Physiotherapy For Rehabilitation And Pain Management</a></li>
          </ul>
        </div>
        <div class="widget widget-call-to-action">
          <h1 class="widget-title">Book Your Appointment</h1>
          <a href="#" class="btn btn-white btn-lg">Make An Appointment</a>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.appointment-footer')
@stop
