@extends('layouts.front-end')
@section('content')
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-caption">
          <h2 class="page-title">Women’s Health</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-breadcrumb">
  <div class="container">
    <div class="col-lg-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('index')}}">Home</a></li>
        <li class="active">Women’s Health</li>
      </ol>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <h1>Women’s Health</h1>
        <img src="{{ url('images/service-single.jpg')}}" alt="" class="img-responsive mb30">
        <p class="lead">Physiotherapy for women mainly addresses issues to the female reproductive system, childbirth, prenatal and postnatal care and issues with infertility.Our specialist physiotherapists assist women with:</p>
          <ol type = 1 class="lead">
            <li>relieving lower back, pelvic and hip pain during and after pregnancy</li>
            <li>strengthening core</li>
            <li>activating pelvic floor</li>
            <li>reducing lymphatic swelling</li>
            <li>controlling urinary incontinence</li>
          </ol>
        <p class="lead">We also help clients with:</p>
          <ol class="lead" type = 1>
            <li>pelvic floor pain & dysfunction</li>
            <li>diastasis recti</li>
            <li>incontinence</li>
            <li>pelvic girdle pain </li>
            <li>pelvic organ prolapse</li>
          </ol>
          <p class="lead">
          We also offer a range of specialist prenatal and postnatal services including pregnancy Pilates classes , physiotherapy sessions and pregnancy massage to help you through all stages along your journey to motherhood.</p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="sidenav">
          <ul class="listnone">
            <li> <a href="{{ route('ppt')}}">Paediatric Physical Therapy</a></li>
            <li> <a href="{{ route('npt')}}">Neurological Physical Therapy</a></li>
            <li> <a href="{{ route('gpt')}}">Geriatric Physical Therapy</a></li>
            <li> <a href="{{ route('opt')}}" class="active">Orthopaedic Physical Therapy</a></li>
            <li> <a href="{{ route('women_health') }}">Women Health</a></li>
            <li> <a href="{{ route('pfrapm')}}">Physiotherapy For Rehabilitation And Pain Management</a></li>
          </ul>
        </div>
        <div class="widget widget-call-to-action">
          <h1 class="widget-title">Book Your Appointment</h1>
          <a href="#" class="btn btn-white btn-lg">Make An Appointment</a>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.appointment-footer')
@stop
