@extends('layouts.app')
@section('content')
<div class="content">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-header">
            Appointment #{{ $appointment->id }}
            <a href="{{ route('appointments.index')}}" class="btn btn-md btn-danger float-right">Back To Appointments</a>
          </div>
          <div class="card-body">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <tbody>
                <tr>
                  <th>Name</th>
                  <td>{{ $appointment->first_name }}  {{ $appointment->last_name }}</td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>{{ $appointment->email }}</td>
                </tr>
                <tr>
                  <th>Contact Number</th>
                  <td>{{ $appointment->contact_number }}</td>
                </tr>
                <tr>
                  <th>Date</th>
                  <td>{{ $appointment->date }}</td>
                </tr>
                <tr>
                  <th>Time</th>
                  <td>{{ $appointment->time }}</td>
                </tr>
                <tr>
                  <th>Payment Type</th>
                  <td>{{ $appointment->payment_type }}</td>
                </tr>
                <tr>
                  <th>Payment Status</th>
                  <td>{{ $appointment->payment_status }}</td>
                </tr>
                <tr>
                  <th>Appointment Status</th>
                  <td>{{ $appointment->appointment_status }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
