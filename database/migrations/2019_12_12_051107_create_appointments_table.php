<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('order_id');
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('email')->nullable();
            $table->text('contact_number')->nullable();
            $table->text('customer_id')->nullable();
            $table->text('date')->nullable();
            $table->text('time')->nullable();
            $table->text('amount')->nullable();
            $table->text('appointment_status')->nullable();
            $table->text('payment_type')->nullable();
            $table->text('payment_status')->nullable();
            $table->text('checksum_hash')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
